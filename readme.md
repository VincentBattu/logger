# Logger

Simple lightweight javascript logger inpired by log4j Java library.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Installing
Simply execute 
``` npm i @vbattu/logger```
or if you use yarn 
``` yarn add @vbattu/logger ```

### Quick Start

```
const Logger = require('@vbattu/logger');
const logger = Logger.getLogger('myFileName');
logger.info('My awesome log');

```
This will produce the following output:
``` [2019-3-13 19:35:16] [INFO] [myFileName] - My awesome log' ```

You are free to pass a class to your logger:
``` 
class MyAwesomeClass {};
const logger = Logger.getLogger(MyAwesomeClass);
logger.info('My awesome log');
```
To obtain the following output:
``` [2019-3-13 19:35:16] [INFO] [MyAwesomeClass] - My awesome log' ```

### Levels

The following levels are available (ASC severity):

- Trace
- Debug
- Info
- Warn
- Error
- Fatal

The default logger level is Debug. You have to configure your logger object to display lower severity (Trace) : 
```
const logger = Logger.getLogger('myLogger');
logger.level = Level.Trace;
```
Or globally:
```soon ;)```

### Appenders

### Layouts

## Authors

- [VincentBattu](https://gitlab.com/VincentBattu)
