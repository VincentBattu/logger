import { Level } from "./level";
import { Logger } from ".";

export class LoggingEvent {

    private _date: Date;

    public constructor(private _message: any, private _level: Level, private _logger: Logger) {
        this._date = new Date();
    }

    get date(): Date {
        return this._date;
    }

    get message(): any {
        return this._message;
    }

    get level(): Level {
        return this._level;
    }

    get logger(): Logger {
        return this._logger;
    }
};
