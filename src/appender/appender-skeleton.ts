import { Appender } from './../appender';
import { Layout } from '../layout';
import { LoggingEvent } from '../logging-event';
import { SimpleLayout } from '../layout/simple-layout';

export abstract class AppenderSkeleton implements Appender {

    private _layout: Layout = null;

    public setLayout(layout: Layout): void {
        this._layout = layout;
    }

    protected get layout(): Layout {
        if (this._layout === null) {
            this._layout = new SimpleLayout();
        }
        return this._layout;
    }

    public abstract doAppend(event: LoggingEvent): void;

}