import { AppenderSkeleton } from "./appender-skeleton";
import { LoggingEvent } from "../logging-event";

export class ConsoleAppender extends AppenderSkeleton {

    public doAppend(event: LoggingEvent): void {
        console.log(this.layout.format(event));
    }

};
