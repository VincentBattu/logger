import { Layout } from '../layout';
import { LoggingEvent } from '../logging-event';

export class SimpleLayout implements Layout {

    format(event: LoggingEvent): string {
        return '[' + event.date.toLocaleDateString() + ' ' + event.date.toLocaleTimeString() + '] ' +
        '[' + event.level.levelStr + '] ' +
        '[' + event.logger.name + '] - ' + event.message;
    }  
   
    getHeader(): string {
        return null;
    }


}