import { AppenderAttachable } from './appender-attachable';
import { Level } from './level';
import { LoggerManager } from './logger-manager';
import { Appender } from './appender';
import { LoggingEvent } from './logging-event';
import { ConsoleAppender } from './appender/console-appender';

type Class = { new(...args: any[]): any; };

export class Logger implements AppenderAttachable {

    /**
     * Niveau du logger.
     * Le niveau par défaut est DEBUG
     */
    private level = Level.DEBUG;

    private appenders: Appender[] = [];

    public constructor(private _name: string) {
    }

    public static getLogger(name: string | Class): Logger {
        if (typeof name !== 'string') {
            name = name.name;
        }
        return LoggerManager.getLogger(name);
    }

    public fatal(message: any): void {
        this.log(message, Level.FATAL);
    }

    public error(message: any): void {
        this.log(message, Level.ERROR);
    }

    public warn(message: any): void {
        this.log(message, Level.WARN);
    }

    public info(message: any): void {
        this.log(message, Level.INFO);
    }

    public debug(message: any): void {
        this.log(message, Level.DEBUG);
    }

    public trace(message: any): void {
        this.log(message, Level.TRACE);
    }

    public log(message: any, level: Level) {
        if (level.isGreatherOrEqual(this.level)) {
            for (const appender of this.getAppenders()) {
                appender.doAppend(new LoggingEvent(message, level, this));
            }
        }
    }

    public setLevel(level: Level): void {
        this.level = level;
    }

    public addAppender(appender: Appender): void {
        this.appenders.push(appender);
    }

    public getAppenders(): Appender[] {
        if (this.appenders.length === 0) {
            this.appenders.push(new ConsoleAppender());
        }
        return this.appenders;
    }

    public get name(): string {
        return this._name;
    }
};
