import { Layout } from './layout';
import { LoggingEvent } from './logging-event';

export interface Appender {

    setLayout(layout: Layout): void;

    doAppend(event: LoggingEvent): void;
    
};