import { Appender } from './appender';

export interface AppenderAttachable {

    addAppender(appender: Appender): void;

    getAppenders(): Appender[];
}