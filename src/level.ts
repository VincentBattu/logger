export class Level {

    public static readonly OFF = new Level(Number.MAX_VALUE, "Off", 0);

    public static readonly FATAL = new Level(50000, "FATAL", 0);

    public static readonly ERROR = new Level(40000, "ERROR", 3);

    public static readonly WARN = new Level(30000, "WARN", 4);

    public static readonly INFO = new Level(20000, "INFO", 6);

    public static readonly DEBUG = new Level(10000, "DEBUG", 7);

    public static readonly TRACE = new Level(5000, "TRACE", 7);

    public static readonly ALL = new Level(Number.MIN_VALUE, "ALL", 7);

    private level: number;

    private _levelStr: string;

    private syslogEquivalent: number;

    public constructor(level: number, levelStr: string, syslogEquivalent: number) {
        this.level = level;
        this._levelStr = levelStr;
        this.syslogEquivalent = syslogEquivalent;
    }

    public isGreatherOrEqual(level: Level): boolean {
        return this.level >= level.level;
    }

    public get levelStr() : string {
        return this._levelStr;
    }
};
