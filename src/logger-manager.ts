import { Logger } from './logger';

export class LoggerManager {

    private static loggers = new Map<string, Logger>();

    public static getLogger(name: string): Logger {
        if (!LoggerManager.loggers.has(name)) {
            LoggerManager.loggers.set(name, new Logger(name));
        }
        return LoggerManager.loggers.get(name) as Logger;
    }
};