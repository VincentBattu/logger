import { LoggingEvent } from './logging-event';
export interface Layout {

    format(event: LoggingEvent): string;

    getHeader(): string;
};